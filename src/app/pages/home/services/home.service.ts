import { InewsletterDescription, InewsletterForm } from './../models/Inewsletter';
import { IhomeRow } from '../models/IhomeRow';
import { IhomeCover } from '../models/IhomeCover';
import { ENDPOINTS } from '../../../../endPoints/endPoints';



import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private coverNewsEndPoint: string = ENDPOINTS.coverNews;
  private latestNewsEndPoint: string = ENDPOINTS.latestNewsUrl;

  private newsletterDescriptionUrl: string = ENDPOINTS.newsletterDescription;
  private newsletterUserUrl: string = ENDPOINTS.newsletterUser;

  constructor(private http: HttpClient) {
    /*Empty*/
  }

  public getCover(): Observable<IhomeCover[]> {
    return this.http.get(this.coverNewsEndPoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };

  public getHome(): Observable<IhomeRow[]> {
    return this.http.get(this.latestNewsEndPoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };

  public getFormDescription(): Observable<InewsletterDescription> {
    return this.http.get(this.newsletterDescriptionUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public getUser(): Observable<InewsletterForm> {
    return this.http.get(this.newsletterUserUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );

  }

  public postDataUser(iNewsletterForm: InewsletterForm): Observable<InewsletterForm> {
    return this.http.post(this.newsletterUserUrl, iNewsletterForm).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}

