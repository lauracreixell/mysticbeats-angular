import { HomeComponent } from '../home/home-components/home.component';
import { ResponseNewsletterComponent } from '../response-newsletter/response-components/response-newsletter.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: '',
  component: HomeComponent,
},
{
  path: 'response-newsletter',
  component: ResponseNewsletterComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
