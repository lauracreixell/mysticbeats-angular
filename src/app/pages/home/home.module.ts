import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-components/home.component';
import { NewsletterComponent } from './home-components/newsletter/newsletter.component';
import { CoverNewsComponent } from './home-components/cover-news/cover-news.component';
import { NewsRowComponent } from './home-components/news-row/news-row.component';


@NgModule({
  declarations: [HomeComponent, NewsletterComponent, CoverNewsComponent, NewsRowComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class HomeModule { }
