export interface IhomeRow{
    id: number;
    newsCoverUrl: string;
    newsCoverAlt: string;
    headline: string;
}