export interface IhomeCover{
    id: number;
    newsCoverUrl: string;
    newsCoverAlt: string;
    headline: string;
}