export interface InewsletterDescription{
    newsletterDescription: string;
}

export interface InewsletterForm{
    email: string;
}