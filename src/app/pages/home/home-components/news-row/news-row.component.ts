import { Component, Input, OnInit } from '@angular/core';
import { IhomeRow } from './../../models/IhomeRow';

@Component({
  selector: 'app-news-row',
  templateUrl: './news-row.component.html',
  styleUrls: ['./news-row.component.scss']
})
export class NewsRowComponent implements OnInit {
  @Input()
  public newsRowDetail!: IhomeRow;
  constructor() { }

  ngOnInit(): void {
  }

}
