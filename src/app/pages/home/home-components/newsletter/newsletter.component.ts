import { HomeService } from '../../services/home.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InewsletterDescription, InewsletterForm } from '../../models/Inewsletter';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  public dataNewsletterDescription!: InewsletterDescription;
  public dataForm!: InewsletterForm;
  public userNewsletter!: InewsletterForm;

  public newsletterForm: FormGroup;
  public submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeService,
    private router: Router,
  ) {
    this.homeService.getFormDescription().subscribe(
      (data: InewsletterDescription) => {
        this.dataNewsletterDescription = data;
      }
    );
    this.newsletterForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
    });
  }


  ngOnInit(): void {
  }
  public onSubmit(): void {
    this.submitted = true;
    if (this.newsletterForm.valid) {
      this.postToUser();
      this.newsletterForm.reset;
      this.submitted = false;
      this.router.navigate(['/response-newsletter']);
    }
  }

  public postToUser(): void {
    const iNewsletterForm: InewsletterForm = {
      email: this.newsletterForm.get('email')?.value,
    }
    this.homeService.postDataUser(iNewsletterForm).subscribe((data) => {
      this.userNewsletter = data;
      alert("You've been subscribed to our newsletter successfully :)")
    },
      (err) => {
        console.error(err.message);
      }
    )
  }
}
