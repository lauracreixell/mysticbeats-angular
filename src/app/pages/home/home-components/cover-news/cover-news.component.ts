import { Component, OnInit, Input } from '@angular/core';
import { IhomeCover } from '../../models/IhomeCover';


@Component({
  selector: 'app-cover-news',
  templateUrl: './cover-news.component.html',
  styleUrls: ['./cover-news.component.scss']
})
export class CoverNewsComponent implements OnInit {
  @Input()
  public coverNewsDetail!: IhomeCover;

  constructor() { }

  ngOnInit(): void {
  }

}
