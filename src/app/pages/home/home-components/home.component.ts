import { IhomeRow } from '../models/IhomeRow';
import { IhomeCover } from '../models/IhomeCover';
import { HomeService } from '../services/home.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public dataRow!: IhomeRow[];
  public dataCover!: IhomeCover[];
  public title!: string;

  constructor(private homeService: HomeService) {
    this.title = 'latest news';
  }

  ngOnInit(): void {
    this.getHomeData();
  }

  public getHomeData(): void {
    this.homeService.getCover().subscribe(
      (data: IhomeCover[]) => {
        this.dataCover = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
    this.homeService.getHome().subscribe(
      (data: IhomeRow[]) => {
        this.dataRow = data;
      },
      (err) => {
        console.error(err.message);
      }
    );

  }

}
