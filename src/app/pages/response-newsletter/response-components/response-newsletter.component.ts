import { Component, OnInit } from '@angular/core';

import { InewsletterForm } from './../../home/models/Inewsletter';
import { HomeService } from '../../home/services/home.service';

import { ResponseNewsletterService } from '../services/response-newsletter.service';
import { IresponseNewsletter } from '../models/IresponseNewsletter';


@Component({
  selector: 'app-response-newsletter',
  templateUrl: './response-newsletter.component.html',
  styleUrls: ['./response-newsletter.component.scss']
})
export class ResponseNewsletterComponent implements OnInit {
  public users!: InewsletterForm;
  public dataResponse!: IresponseNewsletter;
  public button: string;

  constructor(
    private homeService: HomeService,
    private responseService: ResponseNewsletterService
  ) {
    this.button = 'Go back home';
  }

  ngOnInit(): void {
    this.getResponseUser();
    this.getResponsetData();
  }
  public getResponseUser(): void {
    this.homeService.getUser().subscribe((data) => {
      this.users = data;
      console.log(this.users);
    })
  }

  public getResponsetData(): void {
    this.responseService.getNewsletterResponse().subscribe(
      (data: IresponseNewsletter) => {
        console.log(data);
        this.dataResponse = data;
      }
    );
  }

}
