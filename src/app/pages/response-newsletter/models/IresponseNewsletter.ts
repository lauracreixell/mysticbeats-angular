export interface IresponseNewsletter {
    messageHeadline: string;
    message: string;
    farewell: string;
}