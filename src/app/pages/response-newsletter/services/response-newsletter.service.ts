import { Injectable } from '@angular/core';

import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { IresponseNewsletter } from '../models/IresponseNewsletter';
import { ENDPOINTS } from '../../../../endPoints/endPoints';



@Injectable({
  providedIn: 'root'
})
export class ResponseNewsletterService {
  private newsletterResponseEndpoint: string = ENDPOINTS.newsletterResponse;
  constructor(private http: HttpClient) {
    /*Empty */
  }

  public getNewsletterResponse(): Observable<IresponseNewsletter> {
    return this.http.get(this.newsletterResponseEndpoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };
}
