import { TestBed } from '@angular/core/testing';

import { ResponseNewsletterService } from './response-newsletter.service';

describe('ResponseNewsletterService', () => {
  let service: ResponseNewsletterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResponseNewsletterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
