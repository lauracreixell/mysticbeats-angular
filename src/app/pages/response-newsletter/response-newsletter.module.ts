import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResponseNewsletterRoutingModule } from './response-newsletter-routing.module';
import { ResponseNewsletterComponent } from '../response-newsletter/response-components/response-newsletter.component';


@NgModule({
  declarations: [ResponseNewsletterComponent],
  imports: [
    CommonModule,
    ResponseNewsletterRoutingModule
  ]
})
export class ResponseNewsletterModule { }
