import { ENDPOINTS } from '../../../../endPoints/endPoints';
import { Iabout } from '../models/Iabout';

import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AboutService {
  private aboutEndPoint: string = ENDPOINTS.aboutUrl;

  constructor(private http: HttpClient) {
    /*Empty */
  }

  public getAbout(): Observable<Iabout> {
    return this.http.get(this.aboutEndPoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };
}
