import { Iabout } from '../models/Iabout';
import { AboutService } from '../services/about.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
public dataAbout!: Iabout;

  constructor(private aboutService: AboutService) { }

  ngOnInit(): void {
    this.getAboutData();
  }

  public getAboutData(): void{
    this.aboutService.getAbout().subscribe(
      (data: Iabout) => {
        console.log(data);
        this.dataAbout = data;
      }
    );
  }

}
