export interface Iabout{
    aboutSection: string;
    aboutDescription: string;
    secondParagraph: string;
    thirdParagraph: string;
    fourthParagraph: string;
}