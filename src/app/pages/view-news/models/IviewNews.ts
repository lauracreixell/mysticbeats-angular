export interface IviewsNews {
    id: number;
    firstImgUrl: string;
    firstImgAlt: string;
    tag: string;
    newsHeadline: string;
    author: string;
    newsText: string;
    newsTextSecond?: string;
    newsTextThird?: string;
    newsTextFourth?: string;
    newsTextFifth?: string;
    publishingDate: string;
    secondImgUrl?: string;
    secondImgAlt?: string;
}