import { IviewsNews } from '../models/IviewNews';
import { ENDPOINTS } from '../../../../endPoints/endPoints';

import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ViewNewsService {
  private viewNewsEndpoint: string = ENDPOINTS.newsUrl;

  constructor(private http: HttpClient) {
    /*Empty */
  }

  public getViewNews(id: number): Observable<IviewsNews> {
    return this.http.get(this.viewNewsEndpoint + id).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
