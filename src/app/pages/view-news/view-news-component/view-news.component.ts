import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IviewsNews } from '../models/IviewNews';
import { ViewNewsService } from '../services/view-news.service';

@Component({
  selector: 'app-view-news',
  templateUrl: './view-news.component.html',
  styleUrls: ['./view-news.component.scss']
})
export class ViewNewsComponent implements OnInit {
  public viewNews!: IviewsNews;
  public viewNewsId!: string | null;
  constructor(
    private viewNewsService: ViewNewsService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getViewNews();
  }
  public getViewNews(): void {
    this.route.paramMap.subscribe((params) => {
      this.viewNewsId = params.get('id');
    });
    this.viewNewsService.getViewNews(Number(this.viewNewsId)).subscribe((data: IviewsNews) => {
      this.viewNews = data;
      console.log(this.viewNews);
    }, (err) => {
      console.error(err.message);
    }
    );
  }
}
