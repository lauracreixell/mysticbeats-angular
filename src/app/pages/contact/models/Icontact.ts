export interface Icontact{
    contactDescription: string;
    contactEmail: string;
    instagramContact: string;
    facebookContact: string;
    twitterContact: string;
    soundcloudContact: string;
}