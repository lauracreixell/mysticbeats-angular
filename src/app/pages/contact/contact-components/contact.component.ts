import { Icontact } from '../models/Icontact';
import { ContactService } from '../services/contact.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public dataContact!: Icontact;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.getContactData();
  }

  public getContactData(): void{
    this.contactService.getContact().subscribe(
      (data: Icontact) => {
        console.log(data);
        this.dataContact = data;
      }
    );
  }

}
