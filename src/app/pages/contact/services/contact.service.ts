import { Icontact } from '../models/Icontact';
import { ENDPOINTS } from '../../../../endPoints/endPoints';

import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private contactEndPoint: string = ENDPOINTS.contactUrl;

  constructor(private http: HttpClient) {
    /*Empty */
  }

  public getContact(): Observable<Icontact> {
    return this.http.get(this.contactEndPoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };
}
