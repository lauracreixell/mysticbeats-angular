export interface InewsList {
    id: number;
    firstImgUrl: string;
    firstImgAlt: string;
    newsHeadline: string;
}