
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news-components/news.component';
import { NewsCardComponent } from './news-components/news-card/news-card.component';



@NgModule({
  declarations: [NewsComponent, NewsCardComponent ],
  imports: [
    CommonModule,
    NewsRoutingModule
  ]
})
export class NewsModule { }
