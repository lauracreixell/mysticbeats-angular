import { Component, HostListener, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';

import { InewsList } from '../models/InewsList';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  public dataNews!: InewsList[];

  pageYoffset = 0;
  @HostListener('window:scroll', ['$event']) onScroll(event: any) {
    this.pageYoffset = window.pageYOffset;
  }

  constructor(private newsService: NewsService,
    private scroll: ViewportScroller) {
  }

  ngOnInit(): void {
    this.getNewsData();
  }

  scrollToTop() {
    this.scroll.scrollToPosition([0, 0]);
  }

  public getNewsData(): void {
    this.newsService.getNews().subscribe(
      (data: InewsList[]) => {
        this.dataNews = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}
