import { Component, Input, OnInit } from '@angular/core';
import { InewsList } from '../../models/InewsList';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {
  @Input()
  public newsCardDetail!: InewsList;
  constructor() { }

  ngOnInit(): void {
  }

}
