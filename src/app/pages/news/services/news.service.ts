import { InewsList } from '../models/InewsList';
import { ENDPOINTS } from '../../../../endPoints/endPoints';

import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private newsEndpoint: string = ENDPOINTS.newsUrl;

  constructor(private http: HttpClient) {
    /*Empty */
  }

  public getNews(): Observable<InewsList[]> {
    return this.http.get(this.newsEndpoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  };
}
