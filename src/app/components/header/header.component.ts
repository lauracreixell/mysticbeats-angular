import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public logo: string;
  public news: string;
  public about: string;
  public contact: string;

  constructor() {
    this.logo = '../../../assets/logo.png';
    this.news = 'news';
    this.about = 'about';
    this.contact = 'contact';
  }

  ngOnInit(): void {
  }

}
