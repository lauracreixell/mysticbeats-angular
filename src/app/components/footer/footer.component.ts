import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public facebook: string;
  public twitter: string;
  public instagram: string;
  public soundcloud: string;

  constructor() {
    this.facebook = '../../../assets/facebook-icon.png';
    this.twitter = '../../../assets/twitter-icon.png';
    this.instagram = '../../../assets/instagram-icon.png';
    this.soundcloud = '../../../assets/soundcloud-icon.png';
  }

  ngOnInit(): void {
  }

}
