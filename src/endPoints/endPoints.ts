export const ENDPOINTS = {
    coverNews: 'http://localhost:3000/CoverNews',
    latestNewsUrl: 'http://localhost:3000/LatestNews',
    newsletterDescription: 'http://localhost:3000/MysticBeatNewsletter',
    newsletterUser: 'http://localhost:3000/NewsletterUser',
    newsletterResponse: 'http://localhost:3000/NewsletterResponse',
    aboutUrl: 'http://localhost:3000/MysticBeatAbout',
    contactUrl: 'http://localhost:3000/MysticBeatContact',
    newsUrl: 'http://localhost:3000/MysticBeatNews/',
}